extends Node2D


enum {
	MORNING,
	DAY,
	EVENING,
	NIGHT
}
@onready var light_animation = $CanvasLayer/LightAnimation
@onready var day_text = $CanvasLayer/Daytext
@onready var player = $Player/Player

var day_count: int

var state:int = MORNING

func _ready():
	Global.gold = 0
	day_count = 0
	morning_state()
		
func morning_state():
	day_count += 1
	day_text.text = "DAY " + str(day_count)
	light_animation.play("sunrise")

func _on_day_night_timeout():
	if state < 3:
		state += 1
	else:
		state = 0
	match state:
		MORNING:
			morning_state()
		EVENING:
			evening_state()
	Signals.emit_signal("day_time", state, day_count)
			
func evening_state():
	light_animation.play("sunset")


