extends Timer

var gold = preload("res://collectibles/gold.tscn")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var goldTemp = gold.instantiate()
	var rng = RandomNumberGenerator.new()
	var randint = randi_range(50, 500)
	goldTemp.position = Vector2(randint, 650)
	add_child(goldTemp)
