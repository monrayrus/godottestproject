extends Node2D

@onready var mobs = $".."
@onready var animation_player = $AnimationPlayer
var goblin_preload = preload("res://mobs/goblin.tscn")
var mushroom_preload = preload("res://mobs/mushroom.tscn")
var spawn_count

	
func _ready():
	Signals.connect("day_time", Callable(self, "_on_time_changed"))
	print(spawn_count)
	
func _on_time_changed(state, day_count):
	spawn_count = 0
	var rng = randi_range(0, 2)
	if state == 1:
		for i in (day_count + rng):
			animation_player.play("spawn")
			await animation_player.animation_finished
			spawn_count += 1
	if spawn_count == day_count + rng:
		animation_player.play("Idle")
		
func enemy_spawn():
	var rng = randi_range(1,2)
	if rng == 1:
		goblin_spawn()
	else:
		mushroom_spawn()
		

func goblin_spawn():
	var goblin = goblin_preload.instantiate()
	goblin.position = Vector2(self.position.x,500)
	mobs.add_child(goblin)
	
func mushroom_spawn():
	var mushroom = mushroom_preload.instantiate()
	mushroom.position = Vector2(self.position.x,500)
	mobs.add_child(mushroom)
