extends Node

signal enemy_attack(enemy_damage, enemy_state)

signal enemy_died(enemy_position, state)

signal day_time(state, day_count)
