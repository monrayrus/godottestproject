extends CharacterBody2D
class_name Enemy

enum {
	IDLE,
	ATTACK,
	CHASE,
	DAMAGE,
	DEATH,
	RECOVER
}
var state: int  = 0:
	set(value):
		state = value
		match state:
			IDLE:
				idle_state()
			ATTACK:
				attack_state()
			DAMAGE:
				damage_state()
			DEATH:
				death_state()
			RECOVER:
				recover_state()

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
@onready var animPLayer = $AnimationPlayer
@onready var sprite = $AnimatedSprite2D
var player = Vector2.ZERO
var direction = Vector2.ZERO
var damage = 20
var move_speed = 150

func _ready():
	state = CHASE

func _physics_process(delta):
	player = Global.player_pos
	if not is_on_floor():
		velocity.y += gravity * delta
	if state == CHASE:
		chase_state()
	move_and_slide()
	
func _on_attack_range_body_entered(_body):
	state = ATTACK

func idle_state ():
	velocity.x = 0
	animPLayer.play("Idle")
	state = CHASE
	
func attack_state():
	velocity.x = 0
	animPLayer.play("Attack")
	await animPLayer.animation_finished
	state = RECOVER

func chase_state():
	animPLayer.play("Walk")
	direction = (player - self.position).normalized()
	if direction.x < 0:
		sprite.flip_h = true
		$attackDirection.rotation_degrees = 180
	else:
		sprite.flip_h = false
		$attackDirection.rotation_degrees = 0
	velocity.x = direction.x * move_speed
		

func _on_hit_box_area_entered(_area):
	Signals.emit_signal("enemy_attack", damage, state)

	
func damage_state():
	velocity.x = 0
	damage_anim()
	animPLayer.play("Damage")
	await animPLayer.animation_finished
	state = IDLE

func death_state():
	velocity.x = 0
	animPLayer.play("Death")
	await animPLayer.animation_finished
	queue_free()
	
func recover_state():
	velocity.x = 0
	animPLayer.play("recover")
	await animPLayer.animation_finished
	if $attackDirection/attackRange.has_overlapping_bodies():
		state = ATTACK
	else:
		state = IDLE



	
func damage_anim():
	direction = (player - self.position).normalized()
	velocity.x = 0
	if direction.x < 0:
		velocity.x += 100
	elif direction.x > 0:
		velocity.x -= 100
	var tween = get_tree().create_tween()
	tween.parallel().tween_property(self, "velocity", Vector2.ZERO, 0.2)

func _on_run_timeout():
	move_speed = move_toward(move_speed, randi_range(120, 170), 100)
