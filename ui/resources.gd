extends CanvasLayer

@onready var gold_text = $Control/PanelContainer/HBoxContainer/GoldText

func _process(delta):
	gold_text.text = str(Global.gold)
