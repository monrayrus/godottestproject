extends CharacterBody2D

const SPEED = 100.0
const JUMP_VELOCITY = -400.0


enum {
	ATTACK,
	ATTACK2,
	ATTACK3,
	RUN,
	MOVE,
	BLOCK,
	SLIDE,
	DAMAGE,
	DEATH
}


# Get the gravity from the project settings to be synced with RigidBody nodes.
var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")
var state = MOVE
var run_speed = 1
var combo = false
var attack_cooldown = false
var damage_basic = 20
var damage_multiplier = 1
var damage_current
var recovery = false

@onready var anim = $AnimatedSprite2D
@onready var animPlayer = $AnimationPlayer
@onready var stats = $Stats
@onready var leaves = $Leaves
@onready var smack = $Sounds/Smack
@onready var animated_sprite_2d: AnimatedSprite2D = $AnimatedSprite2D


func _ready():
	Signals.connect("enemy_attack", Callable(self, "_on_damage_received"), )


func _physics_process(delta):
	if not is_on_floor():
		velocity.y += gravity * delta

	if velocity.y > 0:
		animPlayer.play("fall")
		
	Global.player_damage = damage_basic * damage_multiplier


	match state:
		MOVE:
			move_state()
		ATTACK:
			attack_state()
		ATTACK2:
			attack2_state()
		ATTACK3:
			attack3_state()
		SLIDE:
			slide_state()
		BLOCK:
			block_state()
		DAMAGE:
			damage_state()
		DEATH:
			death_state()

	move_and_slide()
	
	Global.player_pos = self.position

func move_state():
	var direction = Input.get_axis("left", "right")
	if direction:
		velocity.x = direction * SPEED * run_speed
		if velocity.y == 0:
			if run_speed == 1:
				animPlayer.play("walk")
			else:
				animPlayer.play("run")
	else:
		velocity.x = move_toward(velocity.x, 0, SPEED)
		if velocity.y == 0:
			animPlayer.play("idle")
		
	if direction == -1:
		animated_sprite_2d.flip_h = true
		$AttackDirection.rotation_degrees = 180
	elif direction == 1: 
		animated_sprite_2d.flip_h = false
		$AttackDirection.rotation_degrees = 0
	if Input.is_action_pressed("run") and not recovery:
		run_speed = 2
		stats.stamina -= stats.run_cost
	else:
		run_speed = 1
	if Input.is_action_pressed("block"):
		if not recovery:
			if velocity.x == 0:
				if stats.stamina > 1:
					state = BLOCK
			else:
				if recovery == false:
					stats.stamina_cost = stats.slide_cost
					if stats.stamina > stats.stamina_cost:
						state = SLIDE
			
	if Input.is_action_pressed("attack"):
		if not recovery:
			stats.stamina_cost = stats.attack_cost
			if attack_cooldown == false and stats.stamina > stats.stamina_cost:
				Input.action_release("attack")
				state = ATTACK
			
func block_state():
	stats.stamina -= stats.block_cost
	velocity.x = 0
	animPlayer.play("block")
	await animPlayer.animation_finished
	state = MOVE
		
func slide_state():
	animPlayer.play("slide") 
	await animPlayer.animation_finished
	state = MOVE
	
func attack_state():
	stats.stamina_cost = stats.attack_cost
	damage_multiplier = 1
	if Input.is_action_pressed("attack") and combo == true and stats.stamina > stats.stamina_cost:
		Input.action_release("attack")
		state = ATTACK2
	velocity.x = 0
	animPlayer.play("attack")
	await animPlayer.animation_finished
	attack_freeze()
	state = MOVE
	
func attack2_state():
	stats.stamina_cost = stats.attack_cost
	damage_multiplier = 1.2
	if Input.is_action_pressed("attack") and combo == true and  stats.stamina > stats.stamina_cost:
		Input.action_release("attack")
		state = ATTACK3
	animPlayer.play("attack2")
	await animPlayer.animation_finished
	state = MOVE
	
func attack3_state():
	stats.stamina_cost = stats.attack_cost
	damage_multiplier = 2
	animPlayer.play("attack3")
	await animPlayer.animation_finished
	state = MOVE
	
func combo_one():
	combo = true
	await animPlayer.animation_finished
	combo = false

func attack_freeze():
	attack_cooldown = true
	await get_tree().create_timer(0.5).timeout
	attack_cooldown = false
	
func damage_state():
	anim.play("damage")
	await anim.animation_finished
	state = MOVE
	
func death_state():
	velocity.x = 0
	animPlayer.play("death")
	await animPlayer.animation_finished
	queue_free()
	get_tree().change_scene_to_file("res://menu.tscn")
		
func _on_damage_received(enemy_damage, enemy_state):
	smack.play()
	if enemy_state != 4:
		if(state == BLOCK):
			enemy_damage/=2
		elif(state == SLIDE):
			enemy_damage = 0
		else:
			state = DAMAGE
			damage_anim()
		stats.health -= enemy_damage
		if stats.health <= 0:
			stats.health = 0
			state = DEATH
		else:
			state = DAMAGE


func _on_hit_box_area_entered(_area):
	Signals.emit_signal("player_attack", damage_current)


func _on_stats_no_stamina():
	recovery = true
	await get_tree().create_timer(2).timeout
	recovery = false

func damage_anim():
	velocity.x = 0
	animated_sprite_2d.modulate = Color.RED
	if animated_sprite_2d.flip_h == true:
		velocity.x += 100
	else:
		velocity.x -= 100
	var tween = get_tree().create_tween()
	tween.parallel().tween_property(self, "velocity", Vector2.ZERO, 0.2)
	tween.parallel().tween_property(animated_sprite_2d, "modulate", Color(1, 1, 1, 1), 0.2)
	
func steps():
	leaves.emitting = true
	leaves.one_shot = true
