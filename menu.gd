extends Node2D

@onready var buttons = $Buttons


func _on_exit_pressed():
	buttons.play()
	await buttons.finished
	get_tree().quit()


func _on_play_pressed():
	buttons.play()
	await buttons.finished
	get_tree().change_scene_to_file("res://level.tscn")
